var express = require("express")
var router = express.Router()

var usuariosController = require("../controllers/usuarios")

router.get("/", loggedIn, usuariosController.list)

router.get("/create", usuariosController.create_get)
router.post("/create", usuariosController.create)

router.get("/:id/update", loggedIn, usuariosController.update_get)
router.post("/:id/update", loggedIn, usuariosController.update)

router.post("/:id/delete", loggedIn, usuariosController.delete)

function loggedIn(req, res, next){
  if(req.user){
    next()
  }
  else{
    console.log("user sin loguearse")
    res.redirect("/login")
  }
}


module.exports = router