var Bicicleta = require("../../models/bicicleta")
var mongoose = require("mongoose")
const {db} = require("../../models/bicicleta")

exports.bicicleta_list = function(req, res){
  Bicicleta.allBicis(function(err, bicis){
    res.json({bicis: bicis})
  })
}

exports.bicicleta_create = function(req, res){
  var bici = req.body
  var bici = Bicicleta.createInstance(bici.code, bici.color, bici.modelo, [bici.lat, bici.lng])
  Bicicleta.add(bici)

  res.status(200).json({
    bicicletas: bici
  })
}

exports.bicicleta_update = async function(req, res){
  
  await Bicicleta.updateByCode(req.body.code, {
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng]
  })

  res.status(200).json({
    updated: "true"
  })
}

exports.bicicleta_delete = async function(req, res){
  await Bicicleta.removeByCode(req.body.code, function(){})
  res.status(200).json({removedCode: req.body.code})
}