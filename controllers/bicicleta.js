const Bicicleta = require("../models/bicicleta")
//import bicicleta from "../models/bicicleta"

exports.bicicleta_list = function(req, res){
  Bicicleta.allBicis(function(err, bicis){
    res.render("bicicletas/index", {bicis: bicis, title: "Red Bicicletas - Listado", usuario: req.user})
  })
}

exports.bicicleta_create_get = function(req, res){
  res.render("bicicletas/create")
}

exports.bicicleta_create_post = function(req, res){
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng]
  })
  Bicicleta.add(bici, function(){
    res.redirect("/bicicletas")
  })
}

exports.bicicleta_update_get = function(req, res){
  Bicicleta.findByCode(req.params.code, function(err, bici){
    res.render("bicicletas/update", {bici: bici, usuario: req.user})
  })
}

exports.bicicleta_update_post = async function(req, res){
  await Bicicleta.findOneAndUpdate({code: req.params.code},
    {
      code: req.body.code,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [
        req.body.lat,
        req.body.lng
      ]
    }
  )
  res.redirect("/bicicletas")
}

exports.bicicleta_delete_post = function(req, res){
  Bicicleta.removeByCode(req.params.code, function(){
    res.redirect("/bicicletas")
  })
}