var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {type: "2dsphere", sparse: true}
  }
})

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion
  })
}

bicicletaSchema.methods.toString = function(){
  return `code: ${this.code} | color: ${this.color}`
}


bicicletaSchema.statics.allBicis = function(cb){
  return this.find({}, cb);
}

bicicletaSchema.statics.add = async function(bici, cb){
  await bici.save()
  if(cb)
  cb(bici)
}

bicicletaSchema.statics.findByCode = function(code, cb){
  return this.findOne({"code": code}, cb)
}

bicicletaSchema.statics.removeByCode = async function(code, cb){
  cb(await this.deleteOne({code: code}))
}

bicicletaSchema.statics.updateByCode = function(code, cb){
  return this.findOneAndUpdate({code: code}, cb)
}

module.exports = mongoose.model("Bicicleta", bicicletaSchema);

// var Bicicleta = function(id, color, modelo, ubicacion){
//   this.id = id;
//   this.color = color;
//   this.modelo = modelo;
//   this.ubicacion = ubicacion;
// }
