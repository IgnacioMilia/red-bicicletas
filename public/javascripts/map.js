var mymap = L.map('main_map')

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    /*maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'*/
}).addTo(mymap);

var bicis = []

document.getElementsByTagName("body")[0].onload = async function(){
    var token = await document.cookie
    .split('; ')
    .find(row => row.startsWith('_xsrf'))
    .split('=')[1]

    $.ajax({
        dataType: "json",
        url: "/api/bicicletas",
        // agregar token
        headers: {
            "type": "application/json",
            "token": token
        },
        success: function(result){
            bicis = result.bicis
            console.log(result)
            result.bicis.forEach(function(bici){
                L.marker(bici.ubicacion, {title: bici.code}).addTo(mymap)
            })
            if(result.bicis.length > 0){
                mymap.setView(result.bicis[0].ubicacion, 13);
            }
        }
    })
}