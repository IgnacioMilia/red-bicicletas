var mongoose = require("mongoose");
var request = require("request")
var Bicicleta = require("../../models/bicicleta")
var {db} = require("../../bin/www")

var testCode = 123
describe("Testing Bicicletas", function(){

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, scc){
      if(err) console.log(err)
      done();
    })
  })

  describe("GET /api/bicicletas", () => {
    it("status 200", (done) => {
      request.get("http://localhost:3000/api/bicicletas", (err, res, body) => {
        expect(res.statusCode).toBe(200)
        done()
      })
    })
  })

  describe("POST /api/bicicletas/create", () => {
    it("debe existir el nuevo registro y tener las mismas propiedades", (done) => {
      var headers = {"content-type" : "application/json"};
      var aBici = `{"code":${testCode},"color":"probando","modelo":"probando","lat":"12","lng":"3"}`;
      request.post({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/create",
        body: aBici
      },
      function(err, res, body){
        expect(res.statusCode).toBe(200)
        var storedBici = Bicicleta.findByCode(res.body.code);
        expect(storedBici.color).toEqual(res.body.color)
        expect(storedBici.modelo).toEqual(res.body.modelo)
        done()
      })
    })
  })

  describe("PUT /api/bicicletas/update", () => {
    it("se debe actualizar el registro", (done) => {
      var headers = {"content-type" : "application/json"};
      var aBici = `{"code":"${testCode}","color":"updateado","modelo":"probando","lat":"12","lng":"3"}`;
      request.put({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/update",
        body: aBici
      },
      function(err, res, body){
        expect(res.statusCode).toBe(200)
        var updatedBici = Bicicleta.findByCode(res.body.code);
        expect(updatedBici.color).toEqual(res.body.color)
        expect(updatedBici.modelo).toEqual(res.body.modelo)
        done()
      })
    })
  })

  describe("DELETE /api/bicicletas/delete", () => {
    it("se debe eliminar el registro", (done) => {
      var headers = {"content-type" : "application/json"};
      var aBici = `{"code":"${testCode}"}`;
      request.delete({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/delete",
        body: aBici
      },
      function(err, res, body){
        expect(res.statusCode).toBe(200)
        var removedCode = JSON.parse(body).removedCode
        Bicicleta.findByCode(removedCode, function(err, bici){
          expect(bici).toBe(null)
        })
        done()
      })
    })
  })
})
