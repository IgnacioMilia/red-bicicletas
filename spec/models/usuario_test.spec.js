var mongoose = require("mongoose");
// var request = require("request")
var Bicicleta = require("../../models/bicicleta")
var Usuario = require("../../models/usuario")
var Reserva = require("../../models/reserva")
var {db} = require("../../bin/www")

// db setup
describe("Testing Usuarios", function(){

  afterEach(function(done){
    Reserva.deleteMany({}, function(err, ret){
      if(err) console.log(err)
      Usuario.deleteMany({}, function(err, ret){
        if(err) console.log(err)
        Bicicleta.deleteMany({}, function(err, ret){
          if(err) console.log(err)
          done();
        })
      })
    })
  })

  describe("Cuando un usuario reserva una bici", () => {
    it("debe existir la reserva", (done) => {
      const usuario = new Usuario({nombre: "nombreRandom"})
      usuario.save()
      const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana", ubicacion: [1,2]})
      bicicleta.save()

      var hoy = new Date();
      var mañana = new Date();
      mañana.setDate(hoy.getDate()+1)
      usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
        Reserva.find({}).populate("bicicleta").populate("usuario").exec(function(err, reservas){
          expect(reservas.length).toBe(1)
          expect(reservas[0].diasDeReserva()).toBe(2)
          expect(reservas[0].bicicleta.code).toBe(1)
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
          done()
        })
      })
    })
  })

})