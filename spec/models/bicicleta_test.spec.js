var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta")
var {db} = require("../../bin/www")

// db setup
describe("Testing Bicicletas", function(){

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, scc){
      if(err) console.log(err)
      done();
    })
  })

  describe("Bicicleta.createInstance", () => {
    it("Agregamos una", function(done){
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1])
      expect(bici.code).toBe(1)
      expect(bici.color).toBe("verde")
      done();
    });
  })

  describe("Bicicleta.allBicis", () => {
    it("comienza vacía", function(done) {
      Bicicleta.allBicis(function(err, bicis){
        if(err) console.log(err)
        expect(bicis.length).toBe(0)
        done();
      })
    });
  })
  
  describe("Bicicleta.add", () => {
    it("Agregamos una", (done) => {
      Bicicleta.allBicis(function(err, bicis){
        var len = bicis.length
        var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1])
        Bicicleta.add(bici, function(newBici){
          Bicicleta.allBicis(function(err, bicis){
            expect(bicis.length).toBe(len + 1)
            Bicicleta.findByCode(newBici.code, function(err, finded){
              expect(finded.code).toEqual(newBici.code)
              expect(finded.color).toEqual(newBici.color)
              done();
            })
          })
        })
      })
    });
  })

  describe("Bicicleta.findByCode", () => {
    it("Buscamos por código", (done) => {
      var bici =  Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1])
      Bicicleta.add(bici, function(err, data){
        Bicicleta.findByCode(bici.code, function(err, data){
          expect(data.code).toEqual(bici.code);
          done();
        })
      })
    })
  })

  describe("Bicicleta.removeByCode", () => {
    it("Buscamos por código", (done) => {
      var bici =  Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1])
      Bicicleta.add(bici, function(data){
        expect(data.code).toBe(bici.code)
        Bicicleta.removeByCode(data.code, function(data){
          Bicicleta.findByCode(data.code, function(err, data){
            expect(data).toBe(null)
            done();
          })
        })
      })
    })
  })


})






// describe("Bicicleta.findById", () => {
//   it("Debe devolver la bici con id 1", () => {
//     var a = new Bicicleta(1, "rojo", "urbana", [51.505, -0.09])

//     expect(Bicicleta.findById(1)).toEqual(a)

//   });
// })


// describe("Bicicleta.removeById", () => {
//   it("Debe remover la bici con id 1", () => {
//     var a = Bicicleta.findById(1)

//     var len = Bicicleta.allBicis.length
//     Bicicleta.removeById(1)

//     expect(Bicicleta.allBicis.length).toBe(len-1)
//     expect(Bicicleta.findById(1)).not.toEqual(a)

//   });
// })